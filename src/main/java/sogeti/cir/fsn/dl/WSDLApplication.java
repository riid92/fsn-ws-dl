package sogeti.cir.fsn.dl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WSDLApplication {

	public static void main(String[] args) {
		SpringApplication.run(WSDLApplication.class, args);
	}
}
