package sogeti.cir.fsn.dl;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.fx.model.FSNModel;
import sogeti.cir.fsn.fx.service.ModelService;

@RestController
public class MaintController {
	
	@Autowired
	private ModelRepository modelDao;
	
	@Autowired
	private ModelService modelService;
	
	@PostMapping("/upload")
	public String upload(@RequestBody(required=true) FSNModel model) throws IOException {
		FileUtils.writeByteArrayToFile(new File("/models-ws"), model.getFile(), true);
		model.setFile(null);
		modelDao.save(model);
		return "ok";
	}
	
	@GetMapping("/")
	public Iterable<FSNModel> getAll() {
		return modelDao.findAll();
	}
	
	@PostMapping("/predict/{id}")
	public Object predict(@PathVariable long id) throws Exception {
		FSNModel model = modelDao.findOne(id);
		modelService.predic(null, null, null);
		return model;
	}
}