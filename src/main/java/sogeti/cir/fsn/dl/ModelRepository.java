package sogeti.cir.fsn.dl;

import org.springframework.data.neo4j.repository.GraphRepository;

import sogeti.cir.fsn.fx.model.FSNModel;

public interface ModelRepository extends GraphRepository<FSNModel> {
	
}